/*
 * Author:  Karmazyn Patrick
 * Datum:   22.09.19
 * Titel:   Schnitte
 */

module schnitt_waagrecht(breite, hoehe, anzahl, ECHO=0){
    section = breite/anzahl;
    if (ECHO==1){
        echo("Kurze Linie");
        echo(section*0.6);
        echo("Lange Linie");
        echo(section*0.8);
    }
    for(x=[0:section:breite]){
        for(y=[0:2.2:hoehe]){
            if (x<breite){
                // Untere Section
                translate([x, y])
                square([section*0.4, 0.1]);
                translate([x+(section*0.6), y])
                square([section*0.4, 0.1]);

                // Mittlere Section
                if (y+1.1<hoehe){
                    translate([x+(section*0.2), y+1.1])
                    square([section*0.6, 0.1]);
                }
            }
        }
    }
}

module schnitt_senkrecht(breite, hoehe, anzahl, ECHO=0){
    section = hoehe/anzahl;
    if (ECHO==1){
        echo("Kurze Linie");
        echo(section*0.6);
        echo("Lange Linie");
        echo(section*0.8);
    }
    for(x=[0:2.2:breite]){
        for(y=[0:section:hoehe]){
            if (y<hoehe){
                // Linke Section
                translate([x,y])
                square([0.1, section*0.4]);
                translate([x, y+(section*0.6)])
                square([0.1, section*0.4]);

                // Mittlere Section
                if(x+1.1<breite){
                    translate([x+1.1, y+(section*0.2)])
                    square([0.1, section*0.6]);
                }
            }
        }
    }
}