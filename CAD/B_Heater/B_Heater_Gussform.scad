/*
 * Projekt: Vapour Phase Soldering
 * URL:     https://gitlab.com/patka619/vapour-phase-soldering.git
 * Author:  Karmazyn Patrick
 * Datum:   21.11.19
 * Titel:   B_Heater_Gussform
 */

// Konfigurationsdatei
include <../Config.scad>

// Ansicht
$vpr = [45, 0, 45];
$vpt = [180, 0, 40];
$vpd = 1000;

// Bauteil
difference(){
    linear_extrude(C_Wand+0)
    square([C_L + 2*C_Wand, A_B + 2*C_Wand], center=true);
}