/*
 * Projekt: Vapour Phase Soldering
 * URL:     https://gitlab.com/patka619/vapour-phase-soldering.git
 * Author:  Karmazyn Patrick
 * Datum:   30.11.19
 * Titel:   A_Housing
 */

// Konfigurationsdatei
include <../Config.scad>
include <../lib/Schnitte.scad>

// Ansicht
$vpr = [0, 0, 0];
$vpt = [0, 0, 0];
$vpd = 2400;

// Bauteil
difference(){
    // Grundfläche
    square([A_B-2*A_R_holz+A_L+A_R_holz*PI, A_H]);

    // Erster Ausschnitt am Boden
    square([(A_B/2)-A_R_holz-A_R_fuss, A_R_fuss]);
    translate([(A_B/2)-A_R_holz-A_R_fuss, 0])
    circle(r = A_R_fuss);

    // Erster Fuss
    translate([(A_B/2)-A_R_holz, 0])
    schnitt_senkrecht(0.5*(A_R_fuss*PI), A_H, 15);

    // Zweiter Ausschnitt am Boden
    translate([(A_B/2)-A_R_holz+A_R_fuss+0.5*(A_R_fuss*PI), 0])
    circle(r = A_R_fuss);
    translate([(A_B/2)-A_R_holz+A_R_fuss+0.5*(A_R_fuss*PI), 0])
    square([A_L-2*A_R_holz-A_R_fuss, A_R_fuss]);
    translate([(A_B/2)+0.5*(A_R_fuss*PI)+A_L-3*A_R_holz, 0])
    circle(r = A_R_fuss);

    // Zweiter Fuss
    translate([(A_B/2)+0.5*(A_R_fuss*PI)+A_L-3*A_R_holz+A_R_fuss, 0])
    schnitt_senkrecht(0.5*(A_R_fuss*PI), A_H, 15);

    // Dritter Ausschnitt am Boden
    translate([(A_B/2)+(A_R_fuss*PI)+A_L-3*A_R_holz+2*A_R_fuss, 0])
    circle(r = A_R_fuss);
    translate([(A_B/2)+(A_R_fuss*PI)+A_L-3*A_R_holz+2*A_R_fuss, 0])
    square([(A_B/2)-A_R_fuss, A_R_fuss]);

    // Loecher fuer Heaterplatte
    for ( i = [0:C_abs_screw:C_abs_screw*(C_anz_screw-1)]){
        translate([(A_B/2)-A_R_holz+0.5*(A_R_fuss*PI)+C_L_loch+i, C_H_screw])
        circle(d = C_gr_screw);
    }
}