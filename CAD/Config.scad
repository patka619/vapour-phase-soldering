/*
 * Projekt: Vapour Phase Soldering
 * URL:     https://gitlab.com/patka619/vapour-phase-soldering.git
 * Author:  Karmazyn Patrick
 * Datum:   21.11.19
 * Titel:   Config
 */
 
// Variablen
$fn = $preview ? 96 : 960;

// Behealter
BEH_B_innen = 136;
BEH_B_aussen = 161;
BEH_H = 64.4;
BEH_H_kante = 4.4;
BEH_L_innen = 240;
BEH_L_aussen = 264;

// Luefter
LUE_B = 26;
LUE_H = 120;
LUE_abs_lochloch = 105;
LUE_D_oeffnung = 116;

// Heizplatten
HE_B = 14.4;
HE_H = 37.6;

// Rahmen
A_B = BEH_B_aussen + 2*10;
A_H = 250;
A_L = BEH_L_aussen + 2*10;
A_R_holz = 50;
A_R_fuss = 20;
A_gr_screw = 8;

// Fan
B_H_screw = 50;
B_abs_screw = 100;
B_D_leitung = 8;

// Heaterplatte
C_L = 400;
C_H_screw = 120;
C_L_loch = 60;
C_gr_screw = A_gr_screw;
C_abs_screw = 20;
C_anz_screw = 4;
C_Wand = 10;

// Trennwand
D_D_wand = 3;
D_H = 30;
D_D = 30;
D_B_kanal = 10;
D_H_kanal = 5;
D_L_screw = 150;
D_abs_kanal = 25;
D_anz_screw = 3;
D_gr_screw = A_gr_screw;

// Decke


// Boden
F_abs_boden = 30;

// Fuss
H_D_wand = 1;
H_D_holz = 6;
H_D_boden = 2;
H_H_kante = 3;