/*
 * Projekt: Vapour Phase Soldering
 * URL:     https://gitlab.com/patka619/vapour-phase-soldering.git
 * Author:  Karmazyn Patrick
 * Datum:   15.10.19
 * Titel:   H_Fuss
 */

// Konfigurationsdatei
include <../Config.scad>

// Ansicht
$vpr = [45, 0, 45];
$vpt = [0, 0, 0];
$vpd = 550;

// Bauteil
intersection(){
    difference(){
        union(){
            linear_extrude(F_abs_boden+H_D_boden)
            circle(A_R_holz);
            linear_extrude(H_D_boden)
            circle(A_R_holz+H_D_holz);
            linear_extrude(H_H_kante)
            difference(){
            circle(A_R_holz+H_D_holz+H_D_wand);
            circle(A_R_holz+H_D_holz);
            }
        }
        
    }

linear_extrude(F_abs_boden+H_D_boden)
square(A_R_holz+H_D_holz+H_D_wand);
}