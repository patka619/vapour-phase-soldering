/*
 * Projekt: Vapour Phase Soldering
 * URL:     https://gitlab.com/patka619/vapour-phase-soldering.git
 * Author:  Karmazyn Patrick
 * Datum:   15.10.19
 * Titel:   D_Trennwand_Gussform
 */

// Konfigurationsdatei
include <../Config.scad>

// Ansicht
$vpr = [45, 0, 45];
$vpt = [0, 0, 0];
$vpd = 550;

// Bauteil
difference(){
    // Gehaeuse
    linear_extrude(D_D+D_D_wand){
        square([A_B+2*D_D_wand, D_H+2*D_D_wand], center=true);
    }

    translate([0, 0, D_D_wand])
    linear_extrude(D_D){
        square([A_B,D_H], center=true);
    }

    // Schraubenloecher
    translate([-(A_B/2+D_D_wand), (D_H/4), (D_D/2)+D_D_wand])
    rotate([0, 90])
    linear_extrude(A_B+2*D_D_wand){
        circle(d=D_gr_screw);
    }

    translate([-(A_B/2+D_D_wand), -(D_H/4), (D_D/2)+D_D_wand])
    rotate([0, 90])
    linear_extrude(A_B+2*D_D_wand){
        circle(d=D_gr_screw);
    }

    for (i = [A_B/(D_anz_screw+1):A_B/(D_anz_screw+1):A_B]){
        if (i<A_B){
            translate([-(A_B/2)+i, 0, (D_D/2)+D_D_wand])
            rotate([90, 0])
            linear_extrude((D_H/2)+D_D_wand){
                circle(d = D_gr_screw);
            }
        }
    }
}

// Kanal fuer Kabel
translate([-(D_abs_kanal/2+D_B_kanal/2), -(D_H/2)])
linear_extrude(D_D+D_D_wand){
    square([D_B_kanal, D_H_kanal]);
}

translate([D_abs_kanal/2-D_B_kanal/2, -(D_H/2)])
linear_extrude(D_D+D_D_wand){
    square([D_B_kanal, D_H_kanal]);
}