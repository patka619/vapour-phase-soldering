/*
 * Projekt: Vapour Phase Soldering
 * URL:     https://gitlab.com/patka619/vapour-phase-soldering.git
 * Author:  Karmazyn Patrick
 * Datum:   25.09.19
 * Titel:   F_Boden
 */

// Konfigurationsdatei
include <../Config.scad>

// Bauteil
difference(){
    // Grundkoerper
    translate([A_R_holz, A_R_holz])
    minkowski() {
        square([A_B-(2*A_R_holz), A_L-(2*A_R_holz)]);
        circle(A_R_holz);
    }

    // Loecher
    for (i = [A_B/(D_anz_screw+1):A_B/(D_anz_screw+1):A_B]){
        if (i<A_B){
            translate([i, D_L_screw])
            circle(d = D_gr_screw);
        }
    }
}